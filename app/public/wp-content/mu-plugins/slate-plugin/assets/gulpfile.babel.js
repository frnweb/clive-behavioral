"use strict"

import gulp from "gulp"
import cleanCss from "gulp-clean-css"
import plugins from "gulp-load-plugins"
import yargs from "yargs"
import rimraf from "rimraf"
import browser from "browser-sync"
import yaml from "js-yaml"
import fs from "fs"
import named from "vinyl-named"
import webpack2 from "webpack"
import webpackStream from "webpack-stream"
import csso from "gulp-csso"
import uglify from "gulp-uglify"
import UglifyjsWebpackPlugin from "uglifyjs-webpack-plugin"
import uglifyJsOptions from "uglify-js"
import cssnano from "cssnano"
import postcss from "gulp-postcss"
import sourcemaps from 'gulp-sourcemaps'
import imagemin from 'gulp-imagemin'


// Load all Gulp plugins into one variable
const $ = plugins()

// Check for --production flag
const PRODUCTION = !!(yargs.argv.production);

// Load settings from settings.yml
const { COMPATIBILITY } = loadConfig();

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

// Default Task
gulp.task("default", gulp.series(sass, watch))

// SCSS Task
function sass() {
  return gulp
    .src("scss/slate.scss")
    .pipe($.sourcemaps.init())
    .pipe($.sass().on("error", $.sass.logError))
    .pipe($.autoprefixer({ overrideBrowserslist: COMPATIBILITY }))
    .pipe(postcss([cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('css'))
}

// Watch for changes to static assets, php files, sass, and javascript
function watch() {
  gulp.watch("scss/**/*.scss").on("all", sass)
}