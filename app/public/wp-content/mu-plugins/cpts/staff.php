<?php
/**
 * Plugin Name: Staff CPTS Plugin
 * Description: This is the Custom Post Type for staff.
 * Author: M.M.
 * License: GPL2
*/

// Register Custom Post Type
function sl_staff_cpts() {

	$labels = array(
		'name'                  => _x( 'Staff', 'Post Type General Name', 'sl_staff_cpts' ),
		'singular_name'         => _x( 'Staff', 'Post Type Singular Name', 'sl_staff_cpts' ),
		'menu_name'             => __( 'Staff', 'sl_staff_cpts' ),
		'name_admin_bar'        => __( 'Staff', 'sl_staff_cpts' ),
		'archives'              => __( 'Staff Archives', 'sl_staff_cpts' ),
		'attributes'            => __( 'Staff Attributes', 'sl_staff_cpts' ),
		'parent_item_colon'     => __( 'Parent Staff:', 'sl_staff_cpts' ),
		'all_items'             => __( 'All Staff', 'sl_staff_cpts' ),
		'add_new_item'          => __( 'Add New Staff', 'sl_staff_cpts' ),
		'add_new'               => __( 'Add New', 'sl_staff_cpts' ),
		'new_item'              => __( 'New Staff', 'sl_staff_cpts' ),
		'edit_item'             => __( 'Edit Staff', 'sl_staff_cpts' ),
		'update_item'           => __( 'Update Staff', 'sl_staff_cpts' ),
		'view_item'             => __( 'View Staff', 'sl_staff_cpts' ),
		'view_items'            => __( 'View Staff', 'sl_staff_cpts' ),
		'search_items'          => __( 'Search Staff', 'sl_staff_cpts' ),
		'not_found'             => __( 'Not found', 'sl_staff_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_staff_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_staff_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_staff_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_staff_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_staff_cpts' ),
		'insert_into_item'      => __( 'Insert into Staff', 'sl_staff_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Staff', 'sl_staff_cpts' ),
		'items_list'            => __( 'Staff list', 'sl_staff_cpts' ),
		'items_list_navigation' => __( 'Staff list navigation', 'sl_staff_cpts' ),
		'filter_items_list'     => __( 'Filter Staff list', 'sl_staff_cpts' ),
	);
	$args = array(
		'label'                 => __( 'Staff', 'sl_staff_cpts' ),
		'description'           => __( 'Custom Post Type for staff members', 'sl_staff_cpts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'about-us/providers-and-staff','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-id',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_staff_cpts', $args );

}
add_action( 'init', 'sl_staff_cpts', 0 );
?>