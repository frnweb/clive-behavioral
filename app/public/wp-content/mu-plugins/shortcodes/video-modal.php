<?php
// CTA
	function sl_video_modal($atts) {
		$specs = shortcode_atts( array(
			'src'	=> '',
			'img'   => '',
			'text'	=> ''
			), $atts );

		static $i = 0;

		$modal = '<div class="reveal sl_reveal sl_reveal--video" id="sl_video--' . $i .'" data-reveal data-reset-on-close="true"><div class="responsive-embed widescreen"><iframe width="560" height="315" src="" data-src=' . esc_attr($specs['src'] ) . ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div><button class="close-button" data-close aria-label="Close modal" type="button"><span class="fa fa-times"></span></button></div>';

		if (esc_attr($specs['img'] ) != '') {
			return ' <div class="sl_video"><div class="sl_video__image" style="background-image: url('. esc_attr($specs['img'] ) .'"><a class="sl_button sl_button--video" data-open="sl_video--' . $i . '"><div class="sl_button__play"></div>'. esc_attr($specs['text'] ) .'</a></div></div>'. $modal;
		} else {
			return '<a class="sl_button sl_button--primary" data-open="sl_video--' . $i . '">'. esc_attr($specs['text'] ) .'</a>'. $modal;
		}
		
	}
	add_shortcode( 'video-modal', 'sl_video_modal' );
///CTA
?>