<?php
/**
 * Template Name: Staff Page
 * Description: Page template for the Staff Page. Gives access to staff picker fields
 */

$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

Timber::render( array( 'templates/staff-page.twig', 'page.twig' ), $context );
