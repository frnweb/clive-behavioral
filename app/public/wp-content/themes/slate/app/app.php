<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	});
	return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array(
	'templates',
	'views',
	'views/components',
	'views/modules',
	'views/partials'
);

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape? 
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'sl_main_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'sl_main_css' ) );
		parent::__construct();
	}
	/** This is where you can register custom post types. */
	public function register_post_types() {

	}
	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() {

	}

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context( $context ) {
		$context['menu'] = new Timber\Menu('main-menu');
		$context['user_menu'] = new Timber\Menu('user-menu');
		$context['footer_menu'] = new Timber\Menu('footer-menu');
		$context['option'] = get_fields('option');
		$context['modules'] = get_field('modules');
		$context['site'] = $this;
		return $context;
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( new Twig_SimpleFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		return $twig;
	}

	/**
 	* Enqueue stylesheets and scripts for Wordpress to use.
 	*/

	function sl_main_css() {

		wp_enqueue_style('sl-css', get_stylesheet_directory_uri() . '/dist/css/main.css');
		
		// Adding FontAwesome
		wp_enqueue_style( 'theme_fontawesome', 'https://use.fontawesome.com/releases/v5.15.1/css/all.css');

		//Adding Google Fonts
		//wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;1,400;1,600&display=swap', false );

		// Adding Typekit
		wp_enqueue_style( 'typekit', 'https://use.typekit.net/vle4crv.css', false );
	}

	function sl_main_scripts() {

    	// Slate js
		wp_enqueue_script('sl-js', get_template_directory_uri() . '/dist/js/app.min.js', array(), '1.0', true);

	}
	
}

///enables support for an SVG image to be uploaded 
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//remove comments from pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
remove_post_type_support( 'page', 'comments' );
}

add_action('init', 'remove_post_comment_support', 100);

function remove_post_comment_support() {
remove_post_type_support( 'post', 'comments' );
}

new StarterSite();

//I added this enqueue scripts/filters outside of the StarterSite class so that the admin site would have access to the Google Maps visual on the modules. 
function my_theme_add_scripts() {
	wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDk6oIqyQucKtLXAaqatnovLFf6cVHI2_Q', array(), '3', true );
   }
	
   add_action( 'wp_enqueue_scripts', 'my_theme_add_scripts' );
	
   function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyDk6oIqyQucKtLXAaqatnovLFf6cVHI2_Q';
	
	return $api;
	
   }
	
   add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

