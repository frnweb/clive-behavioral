<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

// Footer Logos
$footerlogos = new FieldsBuilder('footer_logos');
	
	//Logos Repeater
	$footerlogos
	    ->addRepeater('logos', [
	      'min' => 1,
	      'max' => 4,
	      'button_label' => 'Add Logo',
	      'layout' => 'block',
	    ])
		    ->addImage('logo', [
				'label' => 'Logo',
				'ui' => $config->ui
			])
	    ->endRepeater();

// Footer Socials Fields
$footersocials = new FieldsBuilder('footer_socials');

	// Checkbox for icons/Links
	$footersocials
		->addCheckbox('socials', [
				'layout' => 'horizontal',
				'toggle' => 1,
		])
			->addChoices(
				['facebook' => 'Facebook'],
				['instagram' => 'Instagram'],
				['google' => 'Google Plus'],
				['pinterest' => 'Pinterest'],
				['twitter' => 'Twitter'],
				['youtube' => 'YouTube'],
				['linkedin' => 'LinkedIn']
			)

	// Url Fields
		->addText('facebook_url', [
			'label' => 'Facebook URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'facebook')
		
		->addText('instagram_url', [
			'label' => 'Instagram URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'instagram')

		->addText('google_url', [
			'label' => 'Google Plus URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'google')

		->addText('pinterest_url', [
			'label' => 'Pinterest URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'pinterest')

		->addText('twitter_url', [
			'label' => 'Twitter URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'twitter')

		->addText('youtube_url', [
			'label' => 'YouTube URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'youtube')

		->addText('linkedin_url', [
			'label' => 'LinkedIn URL',
			'ui' => $config->ui
		])
			->conditional('socials', '==', 'linkedin');
  


// Social Icons Tab
$accreditations = new FieldsBuilder('accreditations');
	
	$accreditations
		
		// Legit Script
			->addGroup('legitscript', [
				'label' => 'Legit Script',
				'label_placement' => 'left'
			])
				->addTrueFalse('legitscript_check', [
					'label' => 'Legit Script?',
					'ui' => 1,
					'wrapper' => ['width' => 30],
				])
				->addTextArea('legitscript_script', [
					'label' => 'Legit Script JS Snippet',
					'ui' => $config->ui,
					'rows' => '4',
					'wrapper' => ['width' => 70]
				])
					->conditional('legitscript_check', '==', 1)
			->endGroup()


		// BBB Script
			->addGroup('bbbscript', [
				'label' => 'BBB'
			])
				->addTrueFalse('bbbscript_check', [
					'label' => 'BBB Script?',
					'label_placement' => 'left',
					'ui' => 1,
					'wrapper' => ['width' => 30],
				])
				->addTextArea('bbbscript_script', [
					'label' => 'BBB Script JS Snippet',
					'rows' => '4',
					'ui' => $config->ui,
					'wrapper' => ['width' => 70]
				])
					->conditional('bbbscript_check', '==', 1)
			->endGroup()

		// BBB Script
			->addGroup('jointcommission', [
				'label' => 'Joint Commission'
			])
				->addTrueFalse('jc_check', [
					'label' => 'Joint Commission?',
					'label_placement' => 'left',
					'ui' => 1,
					'wrapper' => ['width' => 30],
				])
			->endGroup()

		
		// Custom Logos
			->addGroup('customlogos', [
				'label' => 'Custom Logos'
			])
				->addRepeater('customlogos_repeater', [
					'label' => 'Logos',
	        'min' => 1,
	        'max' => 8,
	        'button_label' => 'Add Logo',
	        'layout' => 'block',
			  ])
			  	->addImage('accreditation_logo', [
			  		'wrapper' => ['width' => 40]
			  	])
			  	->addText('accreditation_logo_alt', [
			  		'label' => 'Alt Text',
			  		'wrapper' => ['width' => 60]
			  	])
			->endGroup();
	

// Footer Options Tabs & Fields
$footeroptions = new FieldsBuilder('footer_options');

	$footeroptions
		->setLocation('options_page', '==', 'theme-footer-settings')
		// Footer Logos Tab
		->addTab('Footer Logos', ['placement' => 'left'])
			->addFields($footerlogos)
		// Social Icons Tab
		->addTab('Social Media', ['placement' => 'left'])
			->addFields($footersocials)
		// Accrediations Tab
		->addTab('accreditations', ['placement' => 'left'])
			->addFields($accreditations);

		return $footeroptions;


	
