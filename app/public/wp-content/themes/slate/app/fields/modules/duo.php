<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$duo = new FieldsBuilder('duo');

$duo
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'))
		->addFields(get_field_partial('partials.grid_options'));

$duo
	->addTab('media', ['placement' => 'left'])
		->addSelect('media', [
			'label' => 'Media Type',
			'wrapper' => ['width' => 20]
		])
	  	->addChoices(
		  ['image' => 'Image'],
		  ['video' => 'Video'],
		  ['gallery' => 'Gallery']
		)

		->addSelect('media_position', [
			'label' => 'Media Position',
			'wrapper' => ['width' => 20]
		])
	  	->addChoices(
		  ['left' => 'Media Left'],
		  ['right' => 'Media Right']
		)


 		//Image 
		->addImage('duo_image', ['wrapper' => ['width' => 60]])

		//Video
        ->addWysiwyg('video_embed', [
            'label' => 'Paste Video Embed Shortcode/iFrame',
        	])
        	->setInstructions('[embed src="embed url goes here"]')
		->conditional('media', '==', 'video' )

  
  	->addTab('content', ['placement' => 'left'])
  		//Title
		->addText('title', [
			'label' => 'Duo Title'
	    	])
	    	->setInstructions('This is optional. This will appear above the duo')
		// Card
		->addFields(get_field_partial('modules.card'));  
    

return $duo;