<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$staff = new FieldsBuilder('staff_template');

$staff
	->setLocation('post_type', '==', 'page')
		->and('page_template', '==', 'template-staff.php');

$staff
   	->addFlexibleContent('modules', [
   		'button_label' => 'Add Module'
   	])
		->addLayout(get_field_partial('modules.staff'))
		->addLayout(get_field_partial('modules.post_content'))
		->addLayout(get_field_partial('modules.wysiwyg'))
		;

return $staff;