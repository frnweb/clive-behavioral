<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$contact = new FieldsBuilder('contact_fields', ['position' => 'normal']);

$contact
  ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'template-contact.php');
  
$contact
  ->addWysiwyg('alert', [
		'label' => 'Contact Alert Banner',
	]);


return $contact;