import 'babel-polyfill'
import $ from 'jquery'
import 'slick-carousel/slick/slick'

import whatInput from 'what-input'
import chatAgent from './lib/live-agent'
import googleMaps from './lib/google-maps'
import gaEvents from './lib/ga-events'
import slickCarousel from './lib/slick-carousel'
import hamburger from './lib/hamburger'
import contactMobile from './lib/contact-mobile'
import backTop from './lib/back-to-top'
import responsiveEmbed from './lib/responsive-embed'


window.jQuery = $;
window.$ = $;

import Foundation from 'foundation-sites'

$(document).foundation();

// Chat Agent
	//chatAgent()
