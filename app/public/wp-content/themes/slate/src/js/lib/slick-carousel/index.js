const carouselSlider = [];

const carouselDuoSlider = [];

const CarouselImageSlider = [];

$( document ).ready(function() {

var carouselExists = document.getElementsByClassName('sl_carousel');
if (carouselExists.length > 0) {
// if carousel module exists on page

myCarouselData.forEach(function(item){
  if (item.type == 'card') {
    carouselSlider.push(item);
  } else if (item.type == 'duo'){
    carouselDuoSlider.push(item);
  } else {
    carouselImageSlider.push(item);
  }
})

console.log(carouselDuoSlider);

//Carousel Card Type

  carouselSlider.forEach(function(item){
    $( '.sl_slider--' + item.loopIndex ).slick({

      // normal options...
      prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
      nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
      infinite: true,
      slidesToShow: item.slidesLarge,
    
      // the magic
      responsive: [{
    
          breakpoint: 1024,
          settings: {
            slidesToShow: item.slidesMedium,
            infinite: true
          }
    
        }, {
    
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            infinite: true
          }
    
        }]
  });
  })


  //Carousel Duo Type
  carouselDuoSlider.forEach(function(item){
    $( '.sl_slider-duo--' + item.loopIndex ).slick({

        // normal options...
        prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
        nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
        infinite: true,
        slidesToShow: 1
    });
  });
};
})

//slick carousel node module
$(".sl_gallery").slick({

    // normal options...
    infinite: true,
    slidesToShow: 1,
    dots: true,
    prevArrow: `<button class="sl_button--gallery sl_prev"></button>`,
    nextArrow: `<button class="sl_button--gallery sl_next"></button>`,
    cssEase: 'ease-in-out',
  
});