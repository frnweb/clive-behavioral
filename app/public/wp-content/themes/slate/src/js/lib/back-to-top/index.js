import $ from 'jquery'

window.jQuery = $;
window.$ = $;

$(window).scroll(function() {
  if ($(this).scrollTop() > 300)
   {
      $('#sl_backtop').addClass('sl_active');
   }
  else
   {
      $('#sl_backtop').removeClass('sl_active');
   }
});
