<?php
/**
 * Template Name: Contact Page
 * Description: Page template for the Contact Page. Gives access to an alert field
 */

$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

Timber::render( array( 'templates/contact-page.twig', 'page.twig' ), $context );
