<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'MXz+RbIY69+r84vGa1JGtXcKwMtu5RGPyfcIElkmIo2ou7lya8ipTUGfBs1s7+lYMnAd5oId7AufgWTwg9im0g==');
define('SECURE_AUTH_KEY',  'fOY2XqElsdRTDm7iQuOCwrdfqaFcRMUXRqwrwSr33v1g0ckfZl66OVlqovjt4GMe9u/P/xtOp4CLQV/Fx0YmKg==');
define('LOGGED_IN_KEY',    'g9NKE4cOlffuM5VEsiUt8yeN0YYiDb+/i48Irg/dDgrm0OGvJyCek0OL9pp22y7/g6LaryGBSuibV3QW1LKk/w==');
define('NONCE_KEY',        'aU9ZyujrbochzHcAcN/Wgq9omibJS45JbIb3pVdcKtoOHoPvy0qQ5b6f2SbFk05Md7+jvrzGzNqMoO7Fzz7iVw==');
define('AUTH_SALT',        'IPkTVpA+3aYCwmmWRWUhBABOUYoRqhbBcCQc7j+GbVHxUWQbC8h07IoMZqKokfaZ/CTxwOHsCEQS41MOO0nF6g==');
define('SECURE_AUTH_SALT', '9hNv3+WLpYucagup3ArWYtpFzIgnWieriXMm8T/pMrv2RBkzpkBeV2+9w1uZGYJOD6OW0J+8HCF1v52nvPn+ug==');
define('LOGGED_IN_SALT',   'AinzAxl94NkvuOwDAT3Y5YgKP18Z20/vWYODIFC+xizR3eQbdKQQuAJd+GyP2NRyoHgC7UFJe7GBGRjOsXyNAQ==');
define('NONCE_SALT',       'yBNwzEMi3QGWg+ICqRHL5GsBBoxnk3wI+dPhEdlX6ciz2ZSlasdaWEHdz8ioEnEPlkxnJPcB/X6kOsJnpe6fXw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
